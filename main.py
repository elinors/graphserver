
from flask import Flask, jsonify, request
from flask_pymongo import PyMongo
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
app.config['MONGO_DBNAME'] = 'mydb'
app.config['MONGO_URI'] = 'mongodb://elinor:san@database-shard-00-00-1f0ec.mongodb.net:27017,database-shard-00-01-1f0ec.mongodb.net:27017,database-shard-00-02-1f0ec.mongodb.net:27017/mydb?ssl=true&replicaSet=dataBase-shard-0&authSource=admin&retryWrites=true'

mongo = PyMongo(app)

@app.route('/students', methods=['GET'])


def get_all_students():
    students = mongo.db.students

    output = []
    data = {}

    cursor = students.find({})
    for document in cursor:

            for i in document['scores']:
                print(i['score'])
                i.update({'name': i['score']})

            output.append({'name': document['name'], '_id': document['_id'], 'children': document['scores']})
    data.update({'name': 'parent'})
    data.update({'children': output})
    return jsonify(data)

if __name__ == '__main__':
    app.run(debug=True)